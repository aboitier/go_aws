package polly

import (
	"encoding/json"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/polly"
	"io"
	"log"
)

type TTSRequest struct {
	LanguageCode    string  `json:"target_polly"`
	VoiceID         string  `json:"voice_id"`
	TextToTranslate *string `json:"text_to_translate"`
	ID              string  `json:"id"`
	S3Link          string  `json:"s3Link"`

	TTL        int64
	AudioText  io.ReadCloser    `json:"-"`
	AwsSession *session.Session `json:"-"`
}

func initTTS(req *TTSRequest, message *string, ID string) {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))
	req.LanguageCode = "fr-CA"
	req.VoiceID = "Chantal"
	req.AwsSession = sess
	req.TextToTranslate = message
	req.ID = ID
}

func TextToSpeech(r *TTSRequest) (*TTSRequest, error) {
	svc := polly.New(r.AwsSession)
	log.Printf("TTS: New Polly client:OK\n")
	input := &polly.SynthesizeSpeechInput{
		LanguageCode: aws.String(r.LanguageCode),
		OutputFormat: aws.String("mp3"),
		Text:         r.TextToTranslate,
		VoiceId:      aws.String(r.VoiceID),
	}
	log.Printf("TTs: Polly-SynthesizeSpeech:OK\n")
	output, err := svc.SynthesizeSpeech(input)
	// manage 00:00 message length
	if err != nil {
		return nil, err
	}
	log.Printf("TTs: svc-SynthesizeSpeech:OK\n")
	r.AudioText = output.AudioStream
	if r.AudioText == nil {
		log.Fatal("AudioText is empty")
	}
	log.Printf("TTs: output-AS:OK\n")
	return r, nil
}
