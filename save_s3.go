package s3

import (
	//	"context"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	//	"io"
	"log"
	"os"
	"time"
)

func generateFilename(id string) string {
	now := time.Now()
	return fmt.Sprintf("%s/%d/%s", now.Month().String(), now.Day(), id)
}

func saveToS3(to_save string) (string, error) {
	svc := s3manager.NewUploader(r.AwsSession)

	bucket := os.Getenv("S3_BUCKET")
	filename := "my_filename"

	output, err := svc.Upload(&s3manager.UploadInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(filename),
		//	Body:        to_save,
		ContentType: aws.String("audio/mpeg"),
		ACL:         aws.String("public-read"),
	})

	if err != nil {
		log.Print(err.Error())
		return "", err
	}
	return output.Location, nil
}
